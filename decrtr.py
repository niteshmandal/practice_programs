def dec(any_func):
    def wrapper():
        print("this is decorator")
        any_func()
    return wrapper

@dec
def func1():
    print("this is function1")

func1()

@dec
def func2():
    print("this is function2")

func2()

#f1=dec(func2)
#f1()   
