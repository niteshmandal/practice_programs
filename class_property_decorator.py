class Mobile:
    def __init__(self,brand,model,price):
        self.brand = brand
        self.model = model
        self._price = max(price,0)

    '''propert decorator'''
    @property
    def specification(self):
        return f"company is {self.brand},Model is {self.model} and price is {self.price}"

    @property
    def price(self):
        return self._price

    @price.setter
    def price(self,new_price):
        self._price = max(new_price,0)


m1=Mobile('lenovo','k3 note',12000)
m2=Mobile('lenevo','k4 note',-15000)
#m2._price=-1000

print(m2.price)

print(m2.specification)
