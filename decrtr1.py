from functools import wraps
def print_function_data(any_function):
    @wraps(any_function)
    def wrapping(*args,**kwargs):
        print(f"it is {any_function.__name__} function")
        print(any_function.__doc__)
        return any_function(*args,**kwargs)
    return wrapping



@print_function_data
def add(a,b):
    """this take two numbers"""
    return a+b

print(add(5,2))
