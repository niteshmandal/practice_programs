with open('read1.html','r') as website:
    with open('read.txt','w') as wf:
        page = website.read()
        link = True
        while link:
            pos = page.find('<a href =')
            if pos == -1:
                link = False
            else:
                first_quote = page.find('\"',pos)
                sec_quote = page.find('\"',first_quote+1)
                url = page[first_quote+1:sec_quote]
                wf.write(url+'\n')
                page= page[sec_quote:]

