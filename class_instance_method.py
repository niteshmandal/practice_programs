class Human:
    def __init__(self,name,surname,age):
        self.first_name = name
        self.last_name = surname
        self.person_age = age

    def full_name(self):
        return f"{self.first_name} {self.last_name}"

    def adult(self):
        return self.person_age > 18


h1=Human('nitesh','mandal',30)
h2=Human('ashish','yadav',31)
h3=Human('ankit','sharma',24)

print(h3.adult())
#print(h2.first_name)
#print(h3.last_name)

'''Used function/method for object/instance''' 
#print(h3.full_name())

#print(h3.adult())
