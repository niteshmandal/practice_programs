l1=[1,2,3,4]

def trip(a):
    return a**3

def my_map(func,l):
    new=[]
    for item in l:
        new.append(func(item))
    return new

def my_map2(func,l):
    return [func(i) for i in l]

#print(my_map(trip,l1))
#print(my_map2(trip,l1))
print(list(map(trip,l1)))
