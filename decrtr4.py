from functools import wraps

def only_int(any_function):
    @wraps(any_function)
    def wrapper(*args,**kwargs):
        if all([type(arg) == int for arg in args]):
            return any_function(*args,**kwargs)
        else:
            return ("invalid int")
       # data_type=[]
       # for arg in args:
       #     data_type.append(type(arg)==int)
       # if all(data_type):
       #     return any_function(*args,**kwargs)
       # else:
       #     return ("invalid int")
    return wrapper


@only_int
def func(*args):
    total = 0
    for arg in args:
        total += arg
    return total

print(func(1,2,3,4,5,[1,2,3]))
