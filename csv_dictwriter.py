#from csv import writer

#with open('myinfo.csv','w') as f:
#    csv_write = writer(f)
#    csv_write.writerow(['name','age'])
#    csv_write.writerow(['nitesh','30'])
#    csv_write.writerow(['kumar','42'])


#    csv_write.writerows([['name','age'],['nitesh','05'],['kumar','15']])

from csv import DictWriter

with open('myinfo.csv','w') as f:
    csv_write = DictWriter(f,fieldnames =['name','last name','age'])
    csv_write.writeheader()
    
    csv_write.writerow({
            'name' : 'Nitesh',
            'age' : 25 ,
            'last name' : 'kumar'
            })

    csv_write.writerows([
            {'name':'nit','last name':'mandal','age':50},
            {'name':'kum','age':68,'last name' :'man'}
            ])
