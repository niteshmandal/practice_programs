class Mobile:
    '''class variable'''
    count_instance = 0
    discount_price = 20
    def __init__(self,company_name,model,price):
            Mobile.count_instance += 1
            self.name = company_name
            self.model = model
            self.price = price

    def discount(self,num):
        offer=(num/100)*self.price
        return self.price-offer

    '''class variable'''
    def discount_var(self):
        offer=(Mobile.discount_price/100)*self.price
        return self.price-offer

    '''object variable'''
    def discount_obj(self):
        offer=(self.discount_price/100)*self.price
        return self.price-offer

    '''class method'''
    @classmethod
    def counts(cls):
        return f"you have called {Mobile.count_instance} instance in {cls.__name__} class"

    '''class method as constructor'''
    @classmethod
    def new_constr(cls,string):
        company,model,price = string.split(',')
        pr=int(price)
        return cls(company,model,pr)

m1=Mobile('lenovo','k3 note',50000)
m2=Mobile('apple','X',50000)
m3=Mobile('Xiaomi','MiA1',50000)
m3.discount_price = 30

m4=Mobile.new_constr('motorola,krt,50000')

print(m3.__dict__)

print(m1.discount(10))

'''class variable'''
print(m2.discount_var())

'''object variable'''
print(m3.discount_obj())

'''example'''
print(Mobile.count_instance)

'''class method'''
print(Mobile.counts())

'''class method as contructor'''
print(m4.discount_var())
