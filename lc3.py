def nestedlist(l):
    l1=[i**2 if (i%2==0) else -i for i in l]
    return l1

l=list(range(10))
print(nestedlist(l))
