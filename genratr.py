def nums(n):
    for i in range(1,n+1):
        yield i

#print(next(nums(10)))

num=nums(10)
#print(next(num))
#print(next(num))
#print(next(num))

for i in num:
    print(i)


#print(next(num))
