f=open('read.txt')

print(f"file name is {f.name}")                 # Data descriptor for name

print(f"cursor is at {f.tell()}th position")    # tell method is used for cursor position

print(f.read(),end='')                          # for reading text file

print(f"cursor is at {f.tell()}th position")

f.seek(0)                                       # seek method to change position of cursor
print(f"cursor is at {f.tell()}th position")

#print(f.readline())                             # to read 1 line at 1 time


lines = f.readlines()                           # returns all lines in list
print(lines)
for line in lines:
    print(line)

f.close()                                       # to close text file
print(f.closed)                                 # Data descriptor for boolean returns
