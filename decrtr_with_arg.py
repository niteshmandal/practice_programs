from functools import wraps

def only_data_type(data_type):
    def types(any_function):
        @wraps(any_function)
        def wrapper(*args,**kwargs):
            if all([type(arg)==data_type for arg in args]):
                return any_function(*args,**kwargs)
            return("invalid type")
        return wrapper
    return types


@only_data_type(str)
def func(*args):
    total = ''
    for i in args:
        total += i
    return total


print(func('nitesh','mandal'))
