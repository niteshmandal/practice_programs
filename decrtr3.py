from functools import wraps
import time

def calculate_time(function):
    @wraps(function)
    def wrapper(*args,**kwargs):
        print(f"executing {function.__name__}")
        t1 = time.time()
        returned_value = function(*args,**kwargs)
        t2 = time.time()
        print(f"time taken {t2-t1}")
        return returned_value
    return wrapper


@calculate_time
def func(n):
    return [i**2 for i in range(1,n+1)]


func(4)
