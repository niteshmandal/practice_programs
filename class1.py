class Human:
    def __init__(self,name,surname,age):
        print('constr called')
        self.first_name = name
        self.last_name = surname
        self.person_age = age


h1=Human('nitesh','mandal',30)
h2=Human('ashish','yadav',31)
h3=Human('ankit','sharma',24)

print(h1.person_age)
print(h2.first_name)
print(h3.last_name)
