from csv import DictReader,DictWriter

with open('my.csv','r') as rf:
    with open('myinfo.csv','w') as wf:
        csv_read = DictReader(rf)
        csv_write = DictWriter(wf,fieldnames = ['name','mail','ph'])
        csv_write.writeheader()
        for row in csv_read:
           # fname,email,phone = row['name'],row['mail'],row['phone']
            csv_write.writerow({
                    'name' : row['name'],
                    'mail': row['mail'],
                    'ph': row['phone']
                    })
